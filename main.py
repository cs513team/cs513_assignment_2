import csv, time, math
import MapObjects as mo
import coordinate_functions as cf

probe_points = []
link_data = []
matched_points = []
not_matched = []

print("Reading Probes from CSV")
with open('Partition6467ProbePoints.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        probe_points.append(mo.ProbePoint(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]))

print("Reading Link Data from CSV")
with open('Partition6467LinkData.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        link_data.append(mo.LinkData(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16]))

print("Calculating angles of each Link point")
for link in link_data:
    coordinates = link.shapeInfo
    for coordinate in coordinates:
        if (coordinates.index(coordinate) < len(coordinates)-1):
            next_coordinate = coordinates[coordinates.index(coordinate)+1]
            coordinate.angle = cf.calculate_angle(coordinate.latitude, coordinate.longitude, next_coordinate.latitude, next_coordinate.longitude)

print("Sorting Links")
size = 100
normalized_links = [[[] for x in range(size)] for y in range(size)]

for link in link_data:
    x = cf.normalize_lat(link.shapeInfo[0].latitude)
    y = cf.normalize_lon(link.shapeInfo[0].longitude)
    normalized_links[x][y].append(link)

print("Calculating nearest nodes")
p_matched = 0
p_not_matched = 0

for probe in probe_points:
    min_dist = 9999
    min_link = None
    min_coord = None
    slope = None
    slope_difference = None
    direction = None
    # Match Probe to Link Node
    for link in normalized_links[cf.normalize_lat(probe.latitude)][cf.normalize_lon(probe.longitude)]:
        for coord in link.shapeInfo:
            dist = 99999
            if coord.angle:
                dist = cf.distance_between_coordinates(probe.latitude, probe.longitude, coord.latitude, coord.longitude) * math.fabs(probe.heading - coord.angle)
            else:
                dist = cf.distance_between_coordinates(probe.latitude, probe.longitude, coord.latitude, coord.longitude)
            if dist < min_dist:
                min_dist = dist
                min_link = link
                min_coord = coord

    if min_link and min_coord:
        p_matched += 1
        distance_from_ref = cf.distance_between_coordinates(probe.latitude, probe.longitude, min_coord.latitude, min_coord.longitude)
        distance_from_link = cf.perpendicular_distance(probe.latitude, probe.longitude, min_link.shapeInfo[0].latitude, min_link.shapeInfo[0].longitude, min_link.shapeInfo[-1].latitude, min_link.shapeInfo[-1].longitude)
        # Calculate direction
        direction = cf.heading_comparison(min_coord.angle, probe.heading)
        # Calculate Slope information
        if min_link.shapeInfo[0].altitude:
            if distance_from_ref != 0:
                slope = (probe.altitude - float(min_link.shapeInfo[0].altitude))/distance_from_ref
            else:
                slope = 0
            if min_link.slopeInfo:
                node_slope = float(min_link.slopeInfo[min_link.shapeInfo.index(min_coord)].slope)
                if (slope + node_slope) != 0:
                    slope_difference = math.fabs((slope - node_slope)/((slope + node_slope)/2))
                else:
                    slope_difference = 0
        matched_points.append(mo.MatchedPoint(probe.sampleID, probe.dateTime, probe.sourceCode, probe.latitude, probe.longitude, probe.altitude, probe.speed, probe.heading, min_link.linkPVID, direction, distance_from_ref, distance_from_link, slope, slope_difference))  
    else:
        p_not_matched += 1
        not_matched.append(probe)
    
    if p_matched%1000 == 0:
        print(p_matched, " probes matched")

print(p_matched, " probes matched, ", p_not_matched, " probes not matched")

print("Writing output CSV file")
with open('MatchedProbesOutput.csv', 'w') as csvfile:
    probe_writer = csv.writer(csvfile, delimiter=',', lineterminator='\n')
    for match in matched_points:
        probe_writer.writerow([match.sampleID, match.dateTime, match.sourceCode, match.latitude, match.longitude, match.altitude, match.speed, match.heading, match.linkPVID, match.direction, match.distFromRef, match.distFromLink, match.calculatedSlope, match.percentDifference])

print("Writing output Slope Data")
with open('SlopeData.csv', 'w') as csvfile:
    probe_writer = csv.writer(csvfile, delimiter=',', lineterminator='\n')
    for match in matched_points:
        if match.calculatedSlope and match.percentDifference:
            probe_writer.writerow([match.calculatedSlope, match.percentDifference])

print("Writing data for unmatched probes")
with open('ProbesNotMatched.csv', 'w') as csvfile:
    probe_writer = csv.writer(csvfile, delimiter=',', lineterminator='\n')
    for nm_probe in not_matched:
        probe_writer.writerow([nm_probe.sampleID, nm_probe.dateTime, nm_probe.sourceCode, nm_probe.latitude, nm_probe.longitude, nm_probe.altitude, nm_probe.speed, nm_probe.heading])