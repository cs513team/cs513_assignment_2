import math

earth_radius = 6371009
radian_const = 180/math.pi

def distance_between_coordinates(lat1, lon1, lat2, lon2):
    return earth_radius * math.acos(math.sin(lat1/radian_const) * math.sin(lat2/radian_const) + math.cos(lat1/radian_const) * math.cos(lat2/radian_const) * math.cos(lon2/radian_const - lon1/radian_const))

def calculate_angle(lat1, lon1, lat2, lon2):
    lon_difference = lon2 - lon1

    x = math.cos(lat2) * math.sin(lon_difference)
    y = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(lon_difference)
    angle = math.atan2(x, y) * radian_const
    return angle

def normalize_lat(coord):
    min_lat = 50.62500
    max_lat = 53.43750
    normalized_coord = ((coord-min_lat) * 99) / (max_lat - min_lat)
    return math.floor(normalized_coord)
    

def normalize_lon(coord):
    min_lon = 8.43751
    max_lon = 11.25000
    normalized_coord = ((coord-min_lon) * 99) / (max_lon - min_lon)
    return math.floor(normalized_coord)

def perpendicular_distance(probeLat, probeLon, linkLat1, linkLon1, linkLat2, linkLon2):
    
    vector_distance=(math.sqrt((linkLat2-linkLat1)**2+(linkLon2-linkLon1)**2))
    if vector_distance != 0:
        perp_dist=math.fabs(((linkLon2-linkLon1)*probeLat-(linkLat2-linkLat1)*probeLon)/vector_distance)
    else:
        perp_dist = 0
    return perp_dist

def calculate_direction(heading):
    side = None
    if heading:
        heading = float(heading)
        side = heading//22.5
    direction = None
    if side==0 or side==15:
        direction = 0
    elif side==1 or side==2:
        direction = 1
    elif side==3 or side==4:
        direction = 2
    elif side==5 or side==6:
        direction = 3
    elif side==7 or side==8:
        direction = 4
    elif side==9 or side==10:
        direction = 5
    elif side==11 or side==12:
        direction = 6
    elif side==13 or side==14:
        direction = 7
    return direction

def heading_comparison(heading1, heading2):
    if calculate_direction(heading1) == calculate_direction(heading2):
        return 'F'
    else:
        return 'T'