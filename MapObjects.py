class ProbePoint:
    __slots__ = ("sampleID", "dateTime", "sourceCode", "latitude", "longitude", "altitude", "speed", "heading")
    def __init__(self, sampleID, dateTime, sourceCode, latitude, longitude, altitude, speed, heading):
        self.sampleID = sampleID
        self.dateTime = dateTime
        self.sourceCode = sourceCode
        self.latitude = float(latitude)
        self.longitude = float(longitude)
        self.altitude = float(altitude)
        self.speed = float(speed)
        self.heading = float(heading)

class Coordinate:
    __slots__ = ("latitude", "longitude", "altitude", "angle")
    def __init__(self, latitude, longitude, altitude=None, angle=None):
        self.latitude = float(latitude)
        self.longitude = float(longitude)
        self.altitude = altitude
        self.angle = angle

class SlopePoint:
    __slots__ = ("distanceFromRef", "slope")
    def __init__(self, distanceFromRef=None, slope=None):
        self.distanceFromRef = distanceFromRef
        self.slope = slope

class LinkData:
    __slots__ = ("linkPVID", "refNodeID", "nrefNodeID", "length", "functionalClass", "directionOfTravel", "speedCategory", "fromRefSpeedLimit", "toRefSpeedLimit", "fromRefNumLanes", "toRefNumLanes", "multiDigitized", "urban", "timeZone", "shapeInfo", "curvatureInfo", "slopeInfo")
    def __init__(self, linkPVID, refNodeID, nrefNodeID, length, functionalClass, directionOfTravel, speedCategory, fromRefSpeedLimit, toRefSpeedLimit, fromRefNumLanes, toRefNumLanes, multiDigitized, urban, timeZone, shapeInfo, curvatureInfo, slopeInfo):
        self.linkPVID = linkPVID
        self.refNodeID = refNodeID
        self.nrefNodeID = nrefNodeID
        self.length = length
        self.functionalClass = functionalClass
        self.directionOfTravel = directionOfTravel
        self.speedCategory = speedCategory
        self.fromRefSpeedLimit = fromRefSpeedLimit
        self.toRefSpeedLimit = toRefSpeedLimit
        self.fromRefNumLanes = fromRefNumLanes
        self.toRefNumLanes = toRefNumLanes
        self.multiDigitized = multiDigitized
        self.urban = urban
        self.timeZone = timeZone
        coordinates = []
        shape_array = shapeInfo.split('|')
        for shape in shape_array:
            point = shape.split('/')
            if(len(point) > 2):
                coordinates.append(Coordinate(point[0], point[1], point[2]))
            else:
                coordinates.append(Coordinate(point[0], point[1]))
        self.shapeInfo = coordinates
        self.curvatureInfo = curvatureInfo
        slopes = []
        if slopeInfo:
            slope_array = slopeInfo.split('|')
            for slope in slope_array:
                point = slope.split('/')
                slopes.append(SlopePoint(point[0], point[1]))
        self.slopeInfo = slopes

class MatchedPoint:
    __slots__ = ("sampleID", "dateTime", "sourceCode", "latitude", "longitude", "altitude", "speed", "heading", "linkPVID", "direction", "distFromRef", "distFromLink", "calculatedSlope", "percentDifference")
    def __init__(self, sampleID, dateTime, sourceCode, latitude, longitude, altitude, speed, heading, linkPVID, direction, distFromRef, distFromLink, calculatedSlope, percentDifference):
        self.sampleID = sampleID
        self.dateTime = dateTime
        self.sourceCode = sourceCode
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.speed = speed
        self.heading = heading
        self.linkPVID = linkPVID
        self.direction = direction
        self.distFromRef = distFromRef
        self.distFromLink = distFromLink
        self.calculatedSlope = calculatedSlope
        self.percentDifference = percentDifference